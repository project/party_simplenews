<?php
/**
 * @file
 * Provides the class for managing subscription settings data sets.
 */

/**
 * Controller class for CRM integration.
 */
class PartySimplenewsSubscriberDataSet extends PartyDefaultDataSet {
  /**
   * var array $email_map
   *   A map of emails to deltas.
   */
  protected $email_map;

  /**
   * Load the mapping of emails to entity ids.
   *
   * @param bool $reset
   *   If true, refresh the cached version.
   *
   * @return array
   *   An array of emails to entity ids.
   */
  public function &getEmailMap($reset = FALSE) {
    if (!isset($this->email_map) || $reset) {
      if (count($this->getEntityIds())) {
        $this->email_map = db_select('simplenews_subscriber', 'ss')
          ->fields('ss', array('mail', 'snid'))
          ->condition('ss.snid', $this->getEntityIds())
          ->execute()
          ->fetchAllKeyed();
      }
      else {
        $this->email_map = array();
      }
    }

    return $this->email_map;
  }

  /**
   * Retrieve an entity id from an email address.
   *
   * @param string $email
   *   The email we want to find a delta for.
   *
   * @return The entity id for the given email or FALSE if it doesn't exist.
   */
  public function getIdByEmail($email) {
    $map = $this->getEmailMap();
    return isset($map[$email]) ? $map[$email]: FALSE;
  }

  /**
   * Retrieve a delta from an email address.
   *
   * @param string $email
   *   The email we want to find a delta for.
   *
   * @return The delta for the given email or FALSE if it doesn't exist.
   */
  public function getDeltaByEmail($email) {
    if (!$id = $this->getIdByEmail($email)) {
      return FALSE;
    }

    return array_search($id, $this->getEntityIds());
  }

  /**
   * Load the full entities based off email.
   *
   * @param array|null $emails
   *   (optional) An array of email(s) to load or NULL to load all entities.
   *
   * @return $this
   */
  public function loadEntitiesByEmail($emails = NULL) {
    $map = $this->getEmailMap();

    if ($emails === NULL) {
      // Load all entities.
      $this->loadEntities();
    }
    elseif (!empty($emails)) {
      // Convert our emails to deltas.
      $deltas = array_flip($this->getEntityIds());
      $deltas = array_intersect_key($deltas, drupal_map_assoc($deltas));

      $this->loadEntities($deltas);
    }

    return $this;
  }

  /**
   * Get a particular attached entity from an email address.
   *
   * @param string $email
   *   (optional) An email to get. If not supplies, it will get the first.
   * @param bool $create
   *   (optional) Create an entity if it doesn't exist. Defaults to FALSE.
   */
  public function getEntityByEmail($email = NULL, $create = FALSE) {
    // Convert an email to a delta.
    $delta = $email ? $this->getDeltaByEmail($email): FALSE;

    if ($delta !== FALSE) {
      $subscriber = $this->getEntity($delta);
    }
    elseif ($create) {
      $subscriber = $this->createEntity($email);
      $this->attachEntity($subscriber);

      // If the subscriber has not had the email set then it's probably been
      // created so set the email.
      $subscriber->mail = $email;
    }
    else {
      return FALSE;
    }

    return $subscriber;
  }

  /**
   * Detach an entity by email address.
   *
   * @param int $email
   *   The email of the entity to detach
   * @param bool $return
   *   Whether you want to return the detached entity or $this for chaining.
   *   Defaults to FALSE.
   *
   * @return object|$this
   *   Depending on $return, either the detached entity or this for chaining.
   *
   * @see PartyDefaultDataSet::detachEntityByDelta();
   */
  final public function detachEntityByEmail($email, $return = FALSE) {
    // Convert an email to a delta.
    $delta = $email ? $this->getDeltaByEmail($email): NULL;
    return $this->detachEntityByDelta($delta, $return);
  }

  /**
   * Overrides PartyDefaultDataSet::createEntity()
   *
   * Try and maintain Simplenews expected behaviour of one subscriber per email
   * address. Ensure a created subscriber is activated initially.
   */
  public function createEntity($email = '') {
    if (!empty($email)) {
      $subscriber = simplenews_subscriber_load_by_mail($email);
    }

    if(empty($subscriber)) {
      $subscriber = parent::createEntity();
      $subscriber->mail = $email;
      $subscriber->activated = TRUE;
    }

    return $subscriber;
  }
}
