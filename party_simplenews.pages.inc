<?php
/**
 * @file
 *  Party Simplenews Pages
 */

/**
 * Party subscription modal edit form callback.
 *
 * @param $js
 *   Provided by ctools, TRUE if js is enabled.
 * @param $party
 *   A Party object.
 * @param $party_data_set
 *   The attached entity object to be edited.
 * @param $entity_id
 *   The attached entity's id.
 *
 * @return
 *   Array of ctools commands in JSON format.
 */
function party_simplenews_subscriptions_modal_edit_page($js, $party) {
  ctools_include('ajax');
  ctools_include('modal');

  if(!$party->email) {
    drupal_set_message('This party does not have an email address.', 'error');
    $commands[] = ctools_modal_command_display('Manage Subscriptions', theme_status_messages(array('display' => 'error')));

    return array(
      '#type' => 'ajax',
      '#commands' => $commands,
    );
  }

  $controller = $party->getDataSetController('party_simplenews_subscriber');
  $subscriber = $controller->getEntityByEmail($party->email, TRUE);
  $controller->save();

  $form_state = array(
    'title' => t('Edit subscriptions'),
    'ajax' => TRUE,
    'build_info' => array(
      'args'  => array(
        $subscriber,
      ),
    ),
  );
  $commands = ctools_modal_form_wrapper('party_simplenews_subscriptions_edit_form', $form_state);

  if (!empty($form_state['executed'])) {
    // Overwrite the output if form submission was successfully executed.
    $commands = array();
    $commands[] = ctools_ajax_command_reload();
  }

  return array(
    '#type' => 'ajax',
    '#commands' => $commands,
  );
}

/**
 * Builds subscription form for party_clinks_subscriptions_modal_edit_page
 */
function party_simplenews_subscriptions_edit_form($form, $form_state, $subscriber) {
  $form['#subscriber'] = $subscriber;

  $options = array();
  $default_value = array();

  // Get newsletters for subscription form checkboxes.
  // Newsletters with opt-in/out method 'hidden' will not be listed.
  foreach (simplenews_newsletter_get_visible() as $newsletter) {
    $options[$newsletter->newsletter_id] = check_plain($newsletter->name);
    $default_value[$newsletter->newsletter_id] = FALSE;
  }

  if ($subscriber) {
    $default_value = array_merge($default_value, $subscriber->newsletter_ids);
  }

  $form['subscriptions'] = array(
    '#type' => 'fieldset',
    '#description' => t('Select your newsletter subscriptions.'),
  );
  $form['subscriptions']['newsletters'] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => $default_value,
  );

  $form['subscriptions']['#title'] = t('Current newsletter subscriptions');

  form_load_include($form_state, 'inc', 'simplenews', 'includes/simplenews.subscription');

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 99,
  );
  return $form;
}

function party_simplenews_subscriptions_edit_form_submit($form, &$form_state) {
  $mail = $form['#subscriber']->mail;

  // We first subscribe, then unsubscribe. This prevents deletion of subscriptions
  // when unsubscribed from the
  arsort($form_state['values']['newsletters'], SORT_NUMERIC);
  foreach ($form_state['values']['newsletters'] as $newsletter_id => $checked) {
    if ($checked) {
      simplenews_subscribe($mail, $newsletter_id, FALSE, 'website');
    }
    else {
      simplenews_unsubscribe($mail, $newsletter_id, FALSE, 'website');
    }
  }
}