<?php
/**
 * @file Provides the recipient handler for custom party lists.
 */

$plugin = array(
  'title' => t('List of Parties'),
  'class' => 'SimplenewsRecipientHandlerPartyCustom',
);

/**
 * Handler for getting the list of recipients for a newsletter.
 *
 * @see SimplenewsRecipientHandlerBase
 */
class SimplenewsRecipientHandlerPartyCustom extends SimplenewsRecipientHandlerBase {

  /**
   * @overrides SimplenewsRecipentHandlerBase::__construct().
   *
   * @throws Exception if no newsletter id is provided.
   */
  public function __construct($newsletter, $handler, $settings = array()) {
    if (!isset($settings['party_ids'])) {
      throw new Exception(t('No party_ids array provided for SimplenewsRecipientHandlerPartyCustom.'));
    }

    parent::__construct($newsletter, $handler, $settings);
  }

  /**
   * Overrides SimplenewsRecipientHandlerBase::buildRecipientQuery().
   */
  public function buildRecipientQuery() {
    // @todo: Be more careful with parties with multiple subscribers.
    $query = parent::buildRecipientQuery();
    $query->innerJoin('party_attached_entity', 'pae', '%alias.eid = s.snid AND %alias.data_set = :ds', array(':ds' => 'party_simplenews_subscriber'));
    $query->innerJoin('party', 'p', '%alias.pid = pae.pid');
    $query->condition('p.pid', $this->settings['party_ids']);

    return $query;
  }

  public static function settingsForm($element, $settings) {
    $string = is_array($settings['party_ids']) ? implode(',', $settings['party_ids']) : '';

    $element['party_ids'] = array(
      '#type' => 'textfield',
      '#title' => t('Party Pids'),
      '#description' => t('Enter a comma seperated list of party pids'),
      '#default_value' => $string,
    );

    return $element;
  }

  public static function settingsFormSubmit($element, &$form_state) {
    $values = drupal_array_get_nested_value($form_state['values'], $element['#parents']);

    $settings['party_ids'] = preg_split("/,[\s,]*/", $values['party_ids']);

    return $settings;
  }
}
